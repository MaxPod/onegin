package app

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/MaxPod/onegin/internal/generated/models"
	apiMail "gitlab.com/MaxPod/onegin/internal/generated/restapi/operations/mail"
	"gitlab.com/MaxPod/onegin/internal/mailer"
)

const SendMessagesError = int64(404)

// nolint:gomnd
var SendMessagesOK = int64(200)

// SendMessagesHandler -  обработчик энпойнта send
func (srv *Service) SendMessagesHandler(params apiMail.SendMessagesParams) middleware.Responder {
	var msg []mailer.Message

	for i := range params.Body {
		mMessage := mailer.Message{
			ID:    params.Body[i].ID,
			Email: *params.Body[i].Email,
		}
		msg = append(msg, mMessage)
	}

	err := srv.Mailer.LoadTasks(srv.ctx, msg)
	if err != nil {
		// формируем ошибку
		code := SendMessagesError
		message := err.Error()

		model := models.Error{
			Code:    &code,
			Message: &message,
		}

		responder := apiMail.NewSendMessagesBadRequest()
		responder.Payload = &model

		return responder
	}

	// формируем ответ
	model := models.OK{
		Code: &SendMessagesOK,
	}

	responder := apiMail.NewSendMessagesOK()
	responder.Payload = &model

	return responder
}
