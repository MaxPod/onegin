package app

import (
	"context"

	"gitlab.com/MaxPod/onegin/internal/mailer"
	"gitlab.com/MaxPod/onegin/internal/sender"
	"gitlab.com/MaxPod/onegin/internal/taskrepository"
)

// Service - сервис отправки сообщений
type Service struct {
	ctx    context.Context
	Mailer *mailer.Mailer
}

// New инициализирует сервис
func New(ctx context.Context) *Service {
	tr := taskrepository.NewTaskMapRepository()
	s := sender.NewSenderMock()
	m := mailer.NewMailer(tr, s)

	return &Service{
		ctx:    ctx,
		Mailer: m,
	}
}

func (srv *Service) OnShutdown() {
	// do smth on shutdown...
}
