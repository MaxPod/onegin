package mailer

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"
)

// Message - сообщение для юзера
type Message struct {
	ID    string
	Email string
}

// Task - задача "сформировать и отправить сообщение"
type Task struct {
	ID    uint32
	MsgID string
	Email string
	Body  []byte
	Stage int
}

const (
	NewMessage     = iota // новое сообщение
	PrepareMessage = iota // подготовленное к отправке
	SendedMessage  = iota // отправленное сообщение

	FailMessage = -1 // неотправленное изза ошибки
)

// TasksRepository - интерфейс хранилища сообщений
type TasksRepository interface {
	SaveTasks(ctx context.Context, messages []Message) (tasksID []uint32, err error)
	GetTask(ctx context.Context, taskID uint32) (Task, error)
	GetListTasks(ctx context.Context, stage int) (tasksID []uint32, err error)
	SaveMessageBody(ctx context.Context, task uint32, mailMessage []byte, stage int) error
	SaveSendResult(ctx context.Context, task uint32, status int, errText string) error
	CancelAllUnfinishedTasks(ctx context.Context) error
}

// Sender - интерфейс клиента - отправителя сообщений
type Sender interface {
	Send(email string, data []byte) error
}

// Mailer - основная сущность сервиса
type Mailer struct {
	tasks        TasksRepository
	sender       Sender
	newMsgQueue  map[string]uint32 // очередь  новых сообщений, ключ - MsgID,  значение - TaskID
	waitMsgQueue map[string]uint32 // очередь сообщений в работе, ключ - MsgID,  значение - TaskID
	failMsgQueue map[string]uint32 // очередь зафейлиных заявок, ключ - MsgID, значение - TaskID
	l            *log.Logger       // ссылка на логеры майлера
	errorLog     *log.Logger
}

// NewMailer - конструктор майлера. При активации удаляет все незавершенные сообщения
func NewMailer(tasks TasksRepository, sender Sender) *Mailer {
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	cancelTaskAmount, err := cancelAllProcessingTask(context.Background(), tasks)
	if err != nil {
		panic(fmt.Errorf("cancelAllProcessingTask:%w", err))
	}

	if cancelTaskAmount > 0 {
		infoLog.Println("отменено обработка ", cancelTaskAmount, "сообщений")
	} else {
		infoLog.Println("ненеобработанных сообщений в репозитории не обранужено")
	}

	newMsgQueue := make(map[string]uint32)
	waitMsgQueue := make(map[string]uint32)
	failMsgQueue := make(map[string]uint32)

	return &Mailer{
		tasks:        tasks,
		sender:       sender,
		l:            infoLog,
		errorLog:     errorLog,
		newMsgQueue:  newMsgQueue,
		waitMsgQueue: waitMsgQueue,
		failMsgQueue: failMsgQueue,
	}
}

// cancelAllProcessingTask - удаляет все незавершенные сообщения из репозитория
func cancelAllProcessingTask(ctx context.Context,
	tasks TasksRepository) (cancelTaskAmount int, err error) {
	unfinishedTasks, err := tasks.GetListTasks(ctx, PrepareMessage)
	if err != nil {
		return 0, fmt.Errorf("tasks.GetListTasks: %w", err)
	}

	//  отмена всех незавершенных и неотправленных задач
	err = tasks.CancelAllUnfinishedTasks(ctx)
	if err != nil {
		return 0, fmt.Errorf("tasks.CancelAllUnfinishedTasks: %w", err)
	}

	cancelTaskAmount = len(unfinishedTasks)

	return cancelTaskAmount, nil
}

// LoadTasks - загружает список адресов для обработки в таблицу задач
func (m *Mailer) LoadTasks(ctx context.Context, Msgs []Message) error {
	// Сюда можно добавить верификацию входных емейлов
	// for i := range Msgs {
	//
	// }
	taskID, err := m.tasks.SaveTasks(ctx, Msgs)
	if err != nil {
		return fmt.Errorf("SaveTasks: %w", err)
	}

	for i := range taskID {
		m.newMsgQueue[Msgs[i].ID] = taskID[i]
	}

	return nil
}

// WriteMessageRun - блокирующая функция, раннер обрабатывающий сообщения
func (m *Mailer) WriteMessageRun(ctx context.Context) error {
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
			waitMsgAmount, err := m.PrepareMessages(ctx)
			if err != nil {
				m.errorLog.Println("PrepareMessages:", err)

				continue
			}

			if waitMsgAmount > 0 {
				m.l.Println("подготовлено к отправке", waitMsgAmount, "писем")
			}
		}
	}
}

// PrepareMessages - подготавливает сообщение для отправки
func (m *Mailer) PrepareMessages(ctx context.Context) (int, error) {
	newTasks, err := m.tasks.GetListTasks(ctx, NewMessage)
	if err != nil {
		return 0, fmt.Errorf("GetListTasks(NewMessage): %w", err)
	}

	waitMsgAmount := 0

	for _, taskID := range newTasks {
		// бизнес-логика формирования сообщения пока простейшая
		mailMessage := []byte("I love Binaryx")
		// фиксируем результат
		err := m.tasks.SaveMessageBody(ctx, taskID, mailMessage, PrepareMessage)
		if err != nil {
			m.errorLog.Println("SaveMessageBody: %w", err)

			continue
		}

		waitMsgAmount++
	}

	return waitMsgAmount, nil
}

// SendMssageRun - блокирущая функция. Отправляет подготовленные сообщения
func (m *Mailer) SendMssageRun(ctx context.Context) error {
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
			sendMsgAmount, err := m.SendMessages(ctx)
			if err != nil {
				m.errorLog.Println("SendMessages:", err)

				continue
			}

			if sendMsgAmount > 0 {
				m.l.Println("отправлено", sendMsgAmount, "писем")
			}
		}
	}
}

// SendMessages - отправляет сообщения через интерфейс Sender
func (m *Mailer) SendMessages(ctx context.Context) (int, error) {
	waitTasksIDList, err := m.tasks.GetListTasks(ctx, PrepareMessage)
	if err != nil {
		return 0, fmt.Errorf("GetListTasks(PrepareMessage): %w", err)
	}

	sendedMsgAmount := 0

	for _, taskID := range waitTasksIDList {
		task, err := m.tasks.GetTask(ctx, taskID)
		if err != nil {
			m.errorLog.Println("GetTask: %w", err)
		}

		err = m.sender.Send(task.Email, task.Body)
		if err != nil {
			m.errorLog.Println("Send: %w", err)

			// фиксируем ошибку отправки
			errSave := m.tasks.SaveSendResult(ctx, taskID, FailMessage, err.Error())
			if errSave != nil {
				m.errorLog.Println("SaveSendResult(err): %w", err)
			}

			// добавляем в очередь фейлов
			m.failMsgQueue[task.MsgID] = taskID

			continue
		}

		// фиксируем результат
		err = m.tasks.SaveSendResult(ctx, taskID, SendedMessage, "")
		if err != nil {
			m.errorLog.Println("SaveSendResult(ok): %w", err)
		}

		sendedMsgAmount++
	}

	return sendedMsgAmount, nil
}
