package taskrepository

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/MaxPod/onegin/internal/mailer"
)

// Row - строка таблицы задач
type Row struct {
	ID      uint32
	MsgID   string
	Email   string
	Body    []byte
	Stage   int
	ErrText string
}

// TaskMap - таблица задач
type TaskMap struct {
	sync.RWMutex
	m map[uint32]*Row
}

// TaskMapRepository - имитатор базы данных с таблицой задач. Ну лень писать работу с ПГ было! :)
type TaskMapRepository struct {
	taskID  uint32
	taskMap *TaskMap
}

// NewTaskMapRepository - конструктор имитатора репозитория
func NewTaskMapRepository() *TaskMapRepository {
	taskMap := TaskMap{m: make(map[uint32]*Row)}

	return &TaskMapRepository{
		taskMap: &taskMap,
	}
}

// SaveTasks - схораняет массив получателей в хранилище, возвращает массив идентификаторов задач
func (t *TaskMapRepository) SaveTasks(ctx context.Context, messages []mailer.Message) (tasksID []uint32, err error) {
	t.taskMap.Lock()
	for _, msg := range messages {
		t.taskID++
		t.taskMap.m[t.taskID] = &Row{
			ID:    t.taskID,
			MsgID: msg.ID,
			Email: msg.Email,
		}
		tasksID = append(tasksID, t.taskID)
	}
	t.taskMap.Unlock()

	return tasksID, nil
}

// GetTask -  получает задачу по идентификатору
func (t *TaskMapRepository) GetTask(ctx context.Context, taskID uint32) (mailer.Task, error) {
	t.taskMap.RLock()
	task, ok := t.taskMap.m[taskID]
	t.taskMap.RUnlock()

	if !ok {
		return mailer.Task{}, fmt.Errorf("Task not found")
	}

	return mailer.Task{
		ID:    task.ID,
		MsgID: task.MsgID,
		Email: task.Email,
		Body:  task.Body,
		Stage: task.Stage,
	}, nil
}

// GetListTasks - получает список задач по типу stage
func (t *TaskMapRepository) GetListTasks(ctx context.Context, stage int) (tasksID []uint32, err error) {
	t.taskMap.RLock()
	for _, task := range t.taskMap.m {
		if task.Stage == stage {
			tasksID = append(tasksID, task.ID)
		}
	}
	t.taskMap.RUnlock()

	return tasksID, nil
}

// SaveMessageBody - сохраняет тело письма в задаче и устанавливает статус "сообщение подготовлено"
func (t *TaskMapRepository) SaveMessageBody(ctx context.Context, taskID uint32, mailMessage []byte, stage int) error {
	t.taskMap.Lock()
	task := t.taskMap.m[taskID]
	task.Body = mailMessage
	task.Stage = stage
	t.taskMap.Unlock()

	return nil
}

// SaveSendResult - сохраняет статус "сообщение отправлено" с ошибкой или без
func (t *TaskMapRepository) SaveSendResult(ctx context.Context, taskID uint32, status int, errText string) error {
	t.taskMap.Lock()
	task := t.taskMap.m[taskID]
	task.Stage = status
	task.ErrText = errText
	t.taskMap.Unlock()

	return nil
}

// CancelAllUnfinishedTasks - отменяет все не отправленные задачи
func (t *TaskMapRepository) CancelAllUnfinishedTasks(ctx context.Context) error {
	t.taskMap.Lock()
	for _, task := range t.taskMap.m {
		if task.Stage == 1 {
			task.Stage = 0
		}
	}
	t.taskMap.Unlock()

	return nil
}
