package sender

import "fmt"

// SenderMock - имитатор отправки сообщения. Реализует интерфейс Mailer.Sender
type SenderMock struct {
	counter int
}

// NewSenderMock - конструктор SenderMock
func NewSenderMock() *SenderMock {
	return &SenderMock{}
}

const failMessageCount = 10

// Send - метод отправки сообщения. Каждое 10 сообщение фейлит ошибкой
func (s *SenderMock) Send(email string, data []byte) error {
	s.counter++

	if s.counter == failMessageCount {
		s.counter = 0

		return fmt.Errorf("Sender error")
	}

	return nil
}
