package main

import (
	"context"
	"log"

	"github.com/go-openapi/loads"
	"gitlab.com/MaxPod/onegin/internal/app"
	"gitlab.com/MaxPod/onegin/internal/config"
	"gitlab.com/MaxPod/onegin/internal/generated/restapi"
	"gitlab.com/MaxPod/onegin/internal/generated/restapi/operations"
	apiMail "gitlab.com/MaxPod/onegin/internal/generated/restapi/operations/mail"
	apptools "gitlab.com/MaxPod/onegin/internal/tools/app"
)

func main() {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	ctx := context.Background()

	srv := app.New(ctx)
	api := operations.NewOneginAPI(swaggerSpec)

	api.MailSendMessagesHandler = apiMail.SendMessagesHandlerFunc(srv.SendMessagesHandler)
	api.ServerShutdown = srv.OnShutdown

	server := restapi.NewServer(api)
	defer server.Shutdown()

	cfg, err := config.InitConfig("onegin")
	if err != nil {
		log.Fatalln(err)
	}

	server.ConfigureAPI()

	server.Port = cfg.HTTPBindPort
	apptools.RunParallel(ctx,
		apptools.SignalNotify,
		srv.Mailer.WriteMessageRun,
		srv.Mailer.SendMssageRun,
		RunServerFunc(server),
	)
}

func RunServerFunc(server *restapi.Server) apptools.RunFunc {
	return func(ctx context.Context) error {
		return apptools.GoAndWaitFnOrCtx(
			ctx,
			server.Serve,
			func() { _ = server.Shutdown() },
		)
	}
}
