module gitlab.com/MaxPod/onegin

go 1.16

require (
	github.com/go-openapi/errors v0.20.1
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.31
	github.com/go-openapi/spec v0.20.3
	github.com/go-openapi/strfmt v0.20.2
	github.com/go-openapi/swag v0.19.15
	github.com/go-openapi/validate v0.20.2
	github.com/jessevdk/go-flags v1.5.0
	github.com/pkg/errors v0.9.1
	github.com/vrischmann/envconfig v1.3.0
	golang.org/x/net v0.0.0-20210917221730-978cfadd31cf
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
)
